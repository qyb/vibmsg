package org.dup2.wear.vibmsg

import android.app.Service
import android.content.Intent
import android.os.*
import android.service.notification.NotificationListenerService
import android.service.notification.StatusBarNotification
import android.util.Log
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.wearable.Wearable
import java.text.SimpleDateFormat
import java.util.*

class NLService : NotificationListenerService() {
    internal var buildDate = BuildConfig.buildTime.toString()
    var mHandler: Handler? = null
    var mLooper: Looper? = null

    inner class ServiceHandler   constructor(looper: Looper) : Handler(looper) { //自动转换过来加的 private 这里需要去掉
        override fun handleMessage(msg: Message) {
            when (msg.what) {

            }
        }
    }

    override fun onCreate() {
        val thread = HandlerThread("NLService")
        thread.start()
        this.mLooper = thread.getLooper()
        var localLooper = this.mLooper
        // 上下两行为了避免 Smart cast to 'Type' is impossible, because 'variable' is a mutable property that could have been changed by this time
        localLooper?.let { this.mHandler = ServiceHandler(localLooper) }
        super.onCreate()
        Log.i(BuildConfig.LogTag, "NLService onCreate $buildDate")
    }

    override fun onDestroy() {
        this.mLooper!!.quit()
        super.onDestroy()
        Log.i(BuildConfig.LogTag, "NLService onDestroy $buildDate")
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        Log.i(BuildConfig.LogTag, "NLService onStartCommand $buildDate")
        return Service.START_NOT_STICKY
    }

    override fun onBind(intent: Intent): IBinder? {
        val r = super.onBind(intent)
        Log.i(BuildConfig.LogTag, "NLService onBind $buildDate")
        return r
    }

    override fun onListenerConnected() {
        Log.i(BuildConfig.LogTag, "NLService onListenerConnected $buildDate")
    }

    override fun onNotificationPosted(sbn: StatusBarNotification) {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val now = Date()
        val key = sbn.key
        val pack = sbn.packageName
        val notif = sbn.notification
        val ticker: String
        val extras: Bundle
        val title: String?
        var text: String
        try {
            ticker = notif.tickerText.toString() //似乎 tickerText 可能是一个 null
            extras = notif.extras
            title = extras.getString("android.title")
            text = extras.getCharSequence("android.text")!!.toString()
        } catch (e: Exception) {
            Log.i(BuildConfig.LogTag, e.message)
            return
        }

        /*
        SQLiteDatabase db = new SQLiteDB(this, "test4.db", null, 1).getWritableDatabase();
        ContentValues val = new ContentValues();
        val.put("pkg", pack);
        val.put("post_at", dateFormat.format(sbn.getPostTime()));
        val.put("proc_at", dateFormat.format(now));
        db.insert("notifmsg", null, val);
        */

        if (pack == "com.tencent.mm") {
            Log.i(BuildConfig.LogTag, buildDate + " 处理微信拦截")
            // 如果用户没有设置了通知显示详情，收到Ticker“微信：你收到了一条消息。”(WeChat: You've received a message.)，否则Ticker为“发消息名: 消息内容”
            // 相应 Title 为“微信”or“人名”
            // extras.text 内容总是包括"XX条"，所以需要用 Ticker 替换
            Log.i(BuildConfig.LogTag, "origText $text")
            text = ticker.replace(title!!, "")
            Log.i(BuildConfig.LogTag, "Ticker $ticker")
            Log.i(BuildConfig.LogTag, "Title $title")
            Log.i(BuildConfig.LogTag, "Text $text")

            val googleApiClient = GoogleApiClient.Builder(this)
                    .addApi(Wearable.API)
                    .build()
            googleApiClient.connect()

            val thread = Thread(Runnable {
                val nodes = Wearable.NodeApi.getConnectedNodes(googleApiClient).await()
                for (node in nodes.nodes) {
                    Log.i(BuildConfig.LogTag, "Find: " + node.displayName)
                    val result = Wearable.MessageApi.sendMessage(googleApiClient, node.id, "/nlservice", "mm".toByteArray()).await()
                    if (!result.status.isSuccess) {
                        Log.i(BuildConfig.LogTag, "INFO ERROR")
                    } else {
                        Log.i(BuildConfig.LogTag, "Success sent to: " + node.displayName)
                    }
                }
            })
            thread.start()
        }

        if (pack == "com.tencent.mm") {
            // 清理状态栏
            this.cancelNotification(key)

            /*
            // Build intent for notification content
            Intent viewIntent = new Intent();
            PendingIntent viewPendingIntent = PendingIntent.getActivity(this, 0, viewIntent, 0);
            */

            /* 看起来能直接通知手表上App震动了... 暂时去掉 Notification
            int notificationId = 001;
            NotificationCompat.Builder notificationBuilder =
                    new NotificationCompat.Builder(this)
                            .setSmallIcon(R.drawable.ic_event)
                            .setAutoCancel(true)
                            .setDefaults(Notification.DEFAULT_ALL)  //.setVibrate(new long[]{1000, 1000, 1000, 1000, 1000, 1000}) 太暴力
                            .setContentTitle("test")
                            .setContentText("test application");
                            //.setContentIntent(viewPendingIntent);

            // Get an instance of the NotificationManager service
            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

            // Build the notification and issues it with notification manager.
            notificationManager.notify(notificationId, notificationBuilder.build());
            */
        }

        if (pack != "com.tencent.mm") {
            Log.i(BuildConfig.LogTag, "NOTIF:$pack:$ticker:$title:$text")
        }
    }
}