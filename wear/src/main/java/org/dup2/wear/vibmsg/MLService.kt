package org.dup2.wear.vibmsg

import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.IBinder
import android.os.Vibrator
import android.util.Log

import com.google.android.gms.wearable.MessageEvent
import com.google.android.gms.wearable.WearableListenerService

class MLService : WearableListenerService() {
    override fun onMessageReceived(messageEvent: MessageEvent?) {
        Log.i("xxx", messageEvent!!.path)
        Log.i("xxx", messageEvent.data.toString())

        val vibrator = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        val vibrationPattern = longArrayOf(0, 200, 50, 150)
        //-1 - don't repeat
        val indexInPatternToRepeat = -1
        vibrator.vibrate(vibrationPattern, indexInPatternToRepeat)
    }
}