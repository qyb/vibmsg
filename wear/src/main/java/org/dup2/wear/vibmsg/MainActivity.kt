package org.dup2.wear.vibmsg

import android.os.Bundle
import android.support.wearable.activity.WearableActivity
import android.support.wearable.view.BoxInsetLayout
import android.view.View
import android.widget.TextView

import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.wearable.MessageApi
import com.google.android.gms.wearable.MessageEvent
import com.google.android.gms.wearable.Wearable

import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale
import android.util.Log
import org.dup2.wear.vibmsg.BuildConfig
import org.dup2.wear.vibmsg.R


class MainActivity : WearableActivity() { //MessageApi.MessageListener{
    /*
    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        Log.i("xxx", messageEvent.getData().toString());
    }*/
    private var mgoogleapiclient: GoogleApiClient? = null
    private var listener: MessageApi.MessageListener? = null

    private var mContainerView: BoxInsetLayout? = null
    private var mTextView: TextView? = null
    private var mClockView: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.i("Info", BuildConfig.buildTime.toString())
        mgoogleapiclient = GoogleApiClient.Builder(this).addApi(Wearable.API).build()
        listener = MessageApi.MessageListener { messageEvent -> Log.i("yyy", messageEvent.data.toString()) }
        Wearable.MessageApi.addListener(mgoogleapiclient, listener)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setAmbientEnabled()

        mContainerView = findViewById<View>(R.id.container) as BoxInsetLayout
        mTextView = findViewById<View>(R.id.text) as TextView
        mClockView = findViewById<View>(R.id.clock) as TextView
    }

    override fun onStart() {
        super.onStart()
        mgoogleapiclient!!.connect()
        Log.i("Info", "onStart")
    }

    override fun onStop() {
        super.onStop()
        Wearable.MessageApi.removeListener(mgoogleapiclient, listener)
        if (mgoogleapiclient != null) {
            mgoogleapiclient!!.disconnect()
        }
    }

    override fun onEnterAmbient(ambientDetails: Bundle?) {
        super.onEnterAmbient(ambientDetails)
        updateDisplay()
    }

    override fun onUpdateAmbient() {
        super.onUpdateAmbient()
        updateDisplay()
    }

    override fun onExitAmbient() {
        updateDisplay()
        super.onExitAmbient()
    }

    private fun updateDisplay() {
        if (isAmbient) {
            Log.i("Info", "update with isAmbient")
            mContainerView!!.setBackgroundColor(resources.getColor(android.R.color.black))
            mTextView!!.setTextColor(resources.getColor(android.R.color.white))
            mClockView!!.visibility = View.VISIBLE

            mClockView!!.text = AMBIENT_DATE_FORMAT.format(Date())
        } else {
            mContainerView!!.background = null
            mTextView!!.setTextColor(resources.getColor(android.R.color.black))
            mClockView!!.visibility = View.GONE
            Log.i("Info", "update without isAmbient")
        }
    }

    companion object {


        private val AMBIENT_DATE_FORMAT = SimpleDateFormat("HH:mm", Locale.US)
    }
}
